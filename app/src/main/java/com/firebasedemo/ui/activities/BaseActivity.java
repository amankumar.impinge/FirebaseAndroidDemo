package com.firebasedemo.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.Toast;

import com.firebasedemo.Constant;
import com.firebasedemo.R;
import com.firebasedemo.User;
import com.firebasedemo.ui.mediafiles.UploadDownloadAudioActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by gaganjoshi on 9/25/17.
 */

public class BaseActivity extends AppCompatActivity implements Constant {
    private ProgressDialog progressDialog;
    protected FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    protected EditText edtEmail, edtPass, edtFirstName, edtLastName, edtMobileNo, edtCountryCode;
    private SharedPreferences preferences;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("TAG", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("TAG", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        preferences=getSharedPreferences("MyPrefs",MODE_PRIVATE);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
    }

    private void setUserId(String userId){
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("USER_ID",userId);
        editor.apply();
    }

    public String getUserId(){
        if (preferences!=null)
        return  preferences.getString("USER_ID","");
        else return "";
    }

    public void showProgress(boolean wantToShow) {
        if (wantToShow)
            progressDialog.show();
        else if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void createUserWithEmailAndPassword(String email, String pass) {
        showProgress(true);
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("TAG", "createUserWithEmail:onComplete:" + task.isSuccessful());

//                       hide the progress
                        showProgress(false);

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            showToast(getString(R.string.auth_failed));
                        } else {
                            showToast(getString(R.string.auth_success));
//                            getCurrentUser();
                            writeMsgToDatabase();

                        }
                        // ...
                    }
                });
    }

    public void signInWithEmailAndPassword(String email, String password, final Activity activity) {
        showProgress(true);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        Log.d("TAG", "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            showProgress(false);
                            Log.w("TAG", "signInWithEmail:failed", task.getException());
                            showToast(getString(R.string.auth_failed));
                            return;
                        }
                        readFromDatabase();




//                       getCurrentUser();
//                        writeMsgToDatabase();
                    }
                });
    }


    protected void writeMsgToDatabase() {
        showProgress(true);

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

//        Creating new user node, which returns the unique key value

        String userId;

//        Create user id with FirebaseAuth

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            userId = firebaseUser.getUid();
        } else
            userId = mDatabase.push().getKey();  //if firebaseUser is null then create the unique userId with this line

        setUserId(userId);
        // creating user object
        User user = new User(edtFirstName.getText().toString(), edtLastName.getText().toString(), edtEmail.getText().toString(), edtMobileNo.getText().toString(), edtCountryCode.getText().toString(),null);

        // pushing user to 'users' node using the userId
        mDatabase.child(userId).setValue(user);

        openMainActivity(user.getFirstName(),user.getLastName(),user.getEmail(),user.getPhoneNo(),user.getCountryCode());

        showProgress(false);
    }

    private void openMainActivity(String firstName,String lastName,String email,String phoneNo,String countryCode){
        showToast(getString(R.string.login_success));
        Intent intent=new Intent(BaseActivity.this,UploadDownloadAudioActivity.class);
        intent.putExtra(firstNameExtra,firstName);
        intent.putExtra(lastNameExtra,lastName);
        intent.putExtra(emailExtra,email);
        intent.putExtra(phoneNoExtra,phoneNo);
        intent.putExtra(countryCodeExtra,countryCode);
        startActivity(intent);
        finish();
    }

    private void readFromDatabase() {
        String userId="";
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            userId = firebaseUser.getUid();

            setUserId(userId);
        // Read from the database
        mDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showProgress(false);
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    Log.d("TAG", "First name: " + user.getFirstName() + ", LastName: " + user.getLastName() + ", phoneNo: " + user.getPhoneNo() + ", email " + user.getEmail() + ", countryCode: " + user.getCountryCode());
                    openMainActivity(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPhoneNo(), user.getCountryCode());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                showProgress(false);
                // Failed to read value
                Log.w("TAG", "Failed to read value.", error.toException());
            }
        });
        }else showToast("user is not authenticated");
    }


    public void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show();
    }

    protected void showAlertDialog(String msg, DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener, boolean isNeutralButtonRequired) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (posListener != null)
            builder.setPositiveButton("Proceed", posListener);
        if (negListener != null)
            builder.setNegativeButton("Cancel", negListener);
        if (isNeutralButtonRequired)
            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        builder.setMessage(msg);
        builder.show();
    }

    //    Show Alert Dialog without Neutral Button
    protected void showAlertDialog(String msg, DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener) {
        showAlertDialog(msg, posListener, negListener, false);
    }

    //    Show Alert Dialog only with Neutral Button
    public void showAlertDialog(String msg) {
        showAlertDialog(msg, null, null, true);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public boolean verify() {
        if (edtEmail != null && edtPass != null && checkConnectivity()) {
            String email = edtEmail.getText().toString();
            String pass = edtPass.getText().toString();

            if (edtFirstName!=null && edtFirstName.getText().toString().equalsIgnoreCase("")) {
                showToast("Please enter the First Name");
                return false;
            }
            if (edtLastName!=null && edtLastName.getText().toString().equalsIgnoreCase("")) {
                showToast("Please enter the last name");
                return false;
            }
            if (email.equalsIgnoreCase("")) {
                showToast("Email can not be empty");
                return false;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showToast("Please enter a vaild email address");
                return false;
            }
            if (pass.equalsIgnoreCase("")) {
                showToast("Password can not be empty");
                return false;
            }
            if (pass.length() <= 5) {
                showToast("Password should have atleast 6 characters");
                return false;
            }
            if (edtMobileNo!=null && edtMobileNo.getText().toString().equalsIgnoreCase("")) {
                showToast("Please enter the Mobile Number");
                return false;
            }
            if (edtCountryCode!=null && edtCountryCode.getText().toString().equalsIgnoreCase("")) {
                showToast("Please enter the country code");
                return false;
            }

            return true;
        }
        showAlertDialog("Please connect to the internet first !");
        return false;
    }

    public boolean checkConnectivity() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void openNewFragment(Fragment fragment){
        FragmentManager fragmentManager=getFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.add(R.id.frag_container,fragment);
        transaction.commit();
    }

    public void openNewActivity(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

}

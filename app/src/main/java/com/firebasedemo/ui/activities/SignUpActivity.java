package com.firebasedemo.ui.activities;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.firebasedemo.R;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private ImageView imgShowPass;
    private boolean isPassVisible;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        edtEmail= (EditText) findViewById(R.id.edt_email);
        edtPass= (EditText) findViewById(R.id.edt_pass);
        edtFirstName= (EditText) findViewById(R.id.edt_first_name);
        edtLastName= (EditText) findViewById(R.id.edt_last_name);
        edtMobileNo= (EditText) findViewById(R.id.edt_mobile_no);
        edtCountryCode= (EditText) findViewById(R.id.edt_country_code);

        imgShowPass= (ImageView) findViewById(R.id.img_show_pass);

        imgShowPass.setOnClickListener(this);
        findViewById(R.id.btn_sign_up).setOnClickListener(this);
        findViewById(R.id.txt_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_sign_up:
                if (verify())
                    createUserWithEmailAndPassword(edtEmail.getText().toString(),edtPass.getText().toString());
                break;

            case R.id.img_show_pass:
                if (!isPassVisible){
                    isPassVisible=true;
                    edtPass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    imgShowPass.setImageResource(R.drawable.hide_pass);
                    edtPass.setSelection(edtPass.getText().length());
                }else {
                    isPassVisible=false;
                    edtPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    imgShowPass.setImageResource(R.drawable.show_pass);
                    edtPass.setSelection(edtPass.getText().length());
                }
                break;

            case R.id.txt_login:
                openNewActivity(LoginActivity.class);
                finish();
                break;
        }
    }
}


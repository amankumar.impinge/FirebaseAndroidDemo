package com.firebasedemo;

/**
 * Created by gaganjoshi on 10/3/17.
 */
public class User {

    public String firstName,lastName,phoneNo,countryCode,email,picture;


    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }

    public User(String firstName,String lastName,String email,String phoneNo,String countryCode) {
        this.firstName = firstName;
        this.lastName=lastName;
        this.phoneNo=phoneNo;
        this.email = email;
        this.countryCode=countryCode;
    }

    public User(String firstName,String lastName,String email,String phoneNo,String countryCode,String picture) {
        this.firstName = firstName;
        this.lastName=lastName;
        this.phoneNo=phoneNo;
        this.email = email;
        this.countryCode=countryCode;
        this.picture=picture;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
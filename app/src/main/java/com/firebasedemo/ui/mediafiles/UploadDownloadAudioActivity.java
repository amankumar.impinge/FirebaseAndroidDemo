package com.firebasedemo.ui.mediafiles;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebasedemo.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class UploadDownloadAudioActivity extends AppCompatActivity implements View.OnClickListener {
    private Button chooseAudio,uploadAudio,downloadAudio;
    ProgressDialog pd;
    Uri uri;
    private TextView txt_audio_name;
    StorageMetadata metadata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_download_audio);
        initview();
        onClicks();

    }

    private void onClicks() {
        chooseAudio.setOnClickListener(this);
        uploadAudio.setOnClickListener(this);
        downloadAudio.setOnClickListener(this);
    }

    private void initview() {
        chooseAudio = (Button)findViewById(R.id.chooseAudio);
        uploadAudio = (Button)findViewById(R.id.uploadAudio);
        downloadAudio = (Button)findViewById(R.id.downloadAudio);
        txt_audio_name = (TextView)findViewById(R.id.txt_audio_name);
    }

    @Override
    public void onClick(View v) {
        if(v == chooseAudio){
            Intent intent_upload = new Intent();
            intent_upload.setType("audio/*");
            intent_upload.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(
                    intent_upload, "Open Audio (mp3) file"), 1);
           // Toast.makeText(UploadDownloadAudioActivity.this, "hi", Toast.LENGTH_SHORT).show();

        }else if(v == uploadAudio){
            pd=new ProgressDialog(this);
            pd.setCancelable(false);
            pd.show();

          uploadAudio();
        }
        else if( v == downloadAudio ){
            pd=new ProgressDialog(this);
            pd.setCancelable(false);
            pd.show();
            downloadAudio();

        }
    }

    private void downloadAudio() {

        FirebaseStorage storage=FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

        storageRef.child("audio/song1").getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                // Use the bytes to display the image
                String path= Environment.getExternalStorageDirectory()+"/song1.mp3";
                try {
                    FileOutputStream fos=new FileOutputStream(path);
                    fos.write(bytes);
                    fos.close();
                    // localFile = File.createTempFile("images", ".jpeg", getExternalFilesDir(null));
                    System.out.println("localFile=" + path);
                    Toast.makeText(UploadDownloadAudioActivity.this, "Success!!!", Toast.LENGTH_SHORT).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e("File Exception ",">>>>>> "+e);
                    Toast.makeText(UploadDownloadAudioActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("Exception ",">>>>>> "+e);

                    Toast.makeText(UploadDownloadAudioActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                pd.dismiss();
                Toast.makeText(UploadDownloadAudioActivity.this, exception.toString()+"!!!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void uploadAudio() {
        // Create the file metadata
        metadata = new StorageMetadata.Builder()
                .setContentType("audio/mpeg")
                .build();

        FirebaseStorage storage=FirebaseStorage.getInstance();
        StorageReference reference=storage.getReference();
        StorageReference imagesRef=reference.child("audio/"+"song1");
        UploadTask uploadTask = imagesRef.putFile(uri, metadata);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                pd.dismiss();
                Toast.makeText(UploadDownloadAudioActivity.this, "Error : "+e.toString(), Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                pd.dismiss();
                Toast.makeText(UploadDownloadAudioActivity.this, "Uploading Done!!!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    protected void onActivityResult(int requestCode,int resultCode,Intent data){

        if(requestCode == 1){

            if(resultCode == RESULT_OK){

                //the selected audio.
                uri = data.getData();
                txt_audio_name.setText(uri.getLastPathSegment());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

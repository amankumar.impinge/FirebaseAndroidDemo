package com.firebasedemo.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;



/**
 * Created by gaganjoshi on 9/25/17.
 */

public class CommonUtils {

    private CommonUtils(){

    }
    private static class CommonUtilsHelper{
        private static final CommonUtils INSTANCE=new CommonUtils();
    }

    public static CommonUtils getInstance(){
        return CommonUtilsHelper.INSTANCE;
    }

    public String changeBitmapIntoBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public Bitmap getBitmapFromBase64(String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

    }



}

package com.firebasedemo.ui.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.system.ErrnoException;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebasedemo.R;
import com.firebasedemo.User;
import com.firebasedemo.ui.fragments.BaseFragment;
import com.firebasedemo.utils.CommonUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {
    private static final int ACTION_REQUEST_GALLERY = 201;
    private static final int ACTION_REQUEST_CAMERA = 202;
    private static final int PIC_CROP = 110;
    protected EditText edtEmail, edtPass, edtFirstName, edtLastName, edtMobileNo, edtCountryCode;
    private CircleImageView circleImageView;
    private Uri initialURI;
    private Uri mCropImageUri;

    private CropImageView mCropImageView;
    private Button btnCropImg;
    private String imgInBase64;
    private Button btnUpdate;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        edtEmail = (EditText) v.findViewById(R.id.edt_email);
        edtPass = (EditText) v.findViewById(R.id.edt_pass);
        edtFirstName = (EditText) v.findViewById(R.id.edt_first_name);
        edtLastName = (EditText) v.findViewById(R.id.edt_last_name);
        edtMobileNo = (EditText) v.findViewById(R.id.edt_mobile_no);
        edtCountryCode = (EditText) v.findViewById(R.id.edt_country_code);
        btnUpdate = (Button) v.findViewById(R.id.btn_update);
        circleImageView = (CircleImageView) v.findViewById(R.id.circle_img);
        readDataFromDatabase(mBaseActivity.getUserId());

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verify())
                    updateDataOnDatabase();
            }
        });

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadImageClick(circleImageView);
            }
        });
    }

    private void readDataFromDatabase(String userId) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
        mDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    Log.d("TAG", "First name: " + user.getFirstName() + ", LastName: " + user.getLastName() + ", phoneNo: " + user.getPhoneNo() + ", email " + user.getEmail() + ", countryCode: " + user.getCountryCode());
                    edtFirstName.setText("" + user.getFirstName());
                    edtLastName.setText("" + user.getLastName());
                    edtMobileNo.setText("" + user.getPhoneNo());
                    edtEmail.setText("" + user.getEmail());
                    edtCountryCode.setText("" + user.getCountryCode());
                    try {
                        if (user.getPicture() != null && !user.getPicture().equalsIgnoreCase(""))
                            circleImageView.setImageBitmap(CommonUtils.getInstance().getBitmapFromBase64(user.getPicture()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("TAG", "Failed to read value.", error.toException());
            }
        });
    }

    public boolean verify() {
        if (edtEmail != null && edtPass != null && mBaseActivity.checkConnectivity()) {
            String email = edtEmail.getText().toString();
            String pass = edtPass.getText().toString();

            if (edtFirstName != null && edtFirstName.getText().toString().equalsIgnoreCase("")) {
                mBaseActivity.showToast("Please enter the First Name");
                return false;
            }
            if (edtLastName != null && edtLastName.getText().toString().equalsIgnoreCase("")) {
                mBaseActivity.showToast("Please enter the last name");
                return false;
            }
            if (email.equalsIgnoreCase("")) {
                mBaseActivity.showToast("Email can not be empty");
                return false;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                mBaseActivity.showToast("Please enter a vaild email address");
                return false;
            }

            if (edtMobileNo != null && edtMobileNo.getText().toString().equalsIgnoreCase("")) {
                mBaseActivity.showToast("Please enter the Mobile Number");
                return false;
            }
            if (edtCountryCode != null && edtCountryCode.getText().toString().equalsIgnoreCase("")) {
                mBaseActivity.showToast("Please enter the country code");
                return false;
            }

            return true;
        }
        mBaseActivity.showAlertDialog("Please connect to the internet first !");
        return false;
    }

    public void updateDataOnDatabase() {
        mBaseActivity.showProgress(true);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
        User user;
        if (imgInBase64 != null) {
            // creating user object
            user = new User(edtFirstName.getText().toString(), edtLastName.getText().toString(), edtEmail.getText().toString(), edtMobileNo.getText().toString(), edtCountryCode.getText().toString(), imgInBase64);
        } else {
            user = new User(edtFirstName.getText().toString(), edtLastName.getText().toString(), edtEmail.getText().toString(), edtMobileNo.getText().toString(), edtCountryCode.getText().toString());
        }
        // pushing user to 'users' node using the userId
        mDatabase.child(mBaseActivity.getUserId()).setValue(user);
        mBaseActivity.showProgress(false);
    }

    /**
     * On load image button click, start pick  image chooser activity.
     */
    public void onLoadImageClick(View view) {
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    /**
     * Crop the image and set it back to the cropping view.
     */
    public void onCropImageClick() {
        Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
        if (cropped != null) {
            circleImageView.setImageBitmap(cropped);
            imgInBase64 = CommonUtils.getInstance().changeBitmapIntoBase64(cropped);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage,
            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                openCroppingDialog(imageUri);
            }
        }
    }

    public void openCroppingDialog(Uri imageUri) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_crop_img);
        mCropImageView = (CropImageView) dialog.findViewById(R.id.CropImageView);
        btnCropImg = (Button) dialog.findViewById(R.id.btn_crop_img);
        mCropImageView.setImageUriAsync(imageUri);
        btnCropImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onCropImageClick();
            }
        });


        dialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openCroppingDialog(mCropImageUri);
        } else {
            Toast.makeText(getActivity(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Create a chooser intent to select the  source to get image from.<br/>
     * The source can be camera's  (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the  intent chooser.
     */
    public Intent getPickImageChooserIntent() {

// Determine Uri of camera image to  save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();

/*// collect all camera intents
        Intent captureIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam =  packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new  Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }*/

// collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

// Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from  {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

}

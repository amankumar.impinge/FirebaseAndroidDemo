package com.firebasedemo;

/**
 * Created by gaganjoshi on 10/3/17.
 */

public interface  Constant {

    String firstNameExtra="FIRST_NAME";
    String lastNameExtra="LAST_NAME";
    String emailExtra="EMAIL";
    String phoneNoExtra="PHONE_NO";
    String countryCodeExtra="COUNTRY_CODE";
}
